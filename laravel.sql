-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-12-2017 a las 04:24:47
-- Versión del servidor: 10.1.28-MariaDB
-- Versión de PHP: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `laravel`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `condominio`
--

CREATE TABLE `condominio` (
  `ID_Condominio` int(11) NOT NULL,
  `NombreCondominio` char(100) NOT NULL,
  `ComunaResponsable` char(50) NOT NULL,
  `Direccion` char(100) NOT NULL,
  `Contacto` char(50) NOT NULL,
  `Telefono` char(15) NOT NULL,
  `HoraAtencion` char(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `condominio`
--

INSERT INTO `condominio` (`ID_Condominio`, `NombreCondominio`, `ComunaResponsable`, `Direccion`, `Contacto`, `Telefono`, `HoraAtencion`) VALUES
(0, 'ProtoCondo', 'Viña', 'Viña', 'Viña', '00', '06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos`
--

CREATE TABLE `datos` (
  `ID_Dato` int(11) NOT NULL,
  `ID_Estanque` int(11) NOT NULL,
  `ID_Recarga` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `porcentajeInicial` decimal(9,4) NOT NULL,
  `porcentajeFinal` decimal(9,4) NOT NULL,
  `totalLitros` decimal(9,4) NOT NULL,
  `consPromDiario` decimal(9,4) NOT NULL,
  `precio` decimal(8,4) NOT NULL,
  `difDias` int(11) NOT NULL,
  `tempMin` decimal(6,3) NOT NULL,
  `PromTempMin` decimal(8,3) NOT NULL,
  `tempMax` decimal(6,3) NOT NULL,
  `PromTempMax` decimal(8,3) NOT NULL,
  `PromPrecio` decimal(8,4) NOT NULL,
  `contDias` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estanque`
--

CREATE TABLE `estanque` (
  `ID_Estanque` int(11) NOT NULL,
  `Capacidad` int(11) NOT NULL,
  `CONDOMINIO_ID_Condominio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `precio`
--

CREATE TABLE `precio` (
  `ID_Precio` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `precio` decimal(8,4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recarga`
--

CREATE TABLE `recarga` (
  `ID_Recarga` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `porcentajeInicial` decimal(4,2) NOT NULL,
  `porcentajeFinal` decimal(4,2) NOT NULL,
  `litrosCargados` decimal(9,3) NOT NULL,
  `totalLitros` decimal(9,3) NOT NULL,
  `ESTANQUE_ID_Estanque` int(11) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temperatura`
--

CREATE TABLE `temperatura` (
  `ID_Temperatura` int(11) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL,
  `fecha` date NOT NULL,
  `tempMinima` decimal(6,3) NOT NULL,
  `tempMax` decimal(6,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `temperatura`
--

INSERT INTO `temperatura` (`ID_Temperatura`, `created_at`, `updated_at`, `deleted_at`, `fecha`, `tempMinima`, `tempMax`) VALUES
(1, '2017-12-07', '2017-12-07', NULL, '2017-12-07', '6.000', '10.000'),
(2, '2017-12-07', '2017-12-07', NULL, '2017-12-13', '0.000', '10.000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Chris Sevilleja', 'chris@scotch.io', '$2y$10$bc20RxXMOxbakC3lKEmKQOAiCd6j2CycavW.6592K6VOvzbUs/R0W', NULL, '2017-11-29 04:30:33', '2017-11-29 04:30:33'),
(2, 'Usuario1', 'usuario1@gmail.com', '$2y$10$bg75uEo8GfQrvVaKJhwy7OMnNYK.u8Z/NygbteC4JlW0L9Ao6CBk.', 'PMw84y0fpcZz0Eiu832bG13IsRgwCadylud8wfVprexC0oqWy2dUlqtplSCS', '2017-11-30 07:00:00', '2017-11-30 07:00:00'),
(3, 'usuario2', 'usuario2@gmail.com', '$2y$10$/jKV4CmLokBCRRJk2wSBuuN8UmgHEd9GXwdiHqS/p9H.pPTSTnO.G', 'syHjXCoLJ6AloV3aHSl1R8nBTkaijiVV9JYNrELeyxqEZIW7hWyrppz8vWcb', '2017-11-30 07:00:38', '2017-11-30 07:00:38');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `condominio`
--
ALTER TABLE `condominio`
  ADD PRIMARY KEY (`ID_Condominio`);

--
-- Indices de la tabla `datos`
--
ALTER TABLE `datos`
  ADD PRIMARY KEY (`ID_Dato`);

--
-- Indices de la tabla `estanque`
--
ALTER TABLE `estanque`
  ADD PRIMARY KEY (`ID_Estanque`),
  ADD KEY `ESTANQUE_CONDOMINIO_FK` (`CONDOMINIO_ID_Condominio`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `precio`
--
ALTER TABLE `precio`
  ADD PRIMARY KEY (`ID_Precio`);

--
-- Indices de la tabla `recarga`
--
ALTER TABLE `recarga`
  ADD PRIMARY KEY (`ID_Recarga`),
  ADD KEY `RECARGA_ESTANQUE_FK` (`ESTANQUE_ID_Estanque`);

--
-- Indices de la tabla `temperatura`
--
ALTER TABLE `temperatura`
  ADD PRIMARY KEY (`ID_Temperatura`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `recarga`
--
ALTER TABLE `recarga`
  MODIFY `ID_Recarga` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `temperatura`
--
ALTER TABLE `temperatura`
  MODIFY `ID_Temperatura` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `estanque`
--
ALTER TABLE `estanque`
  ADD CONSTRAINT `ESTANQUE_CONDOMINIO_FK` FOREIGN KEY (`CONDOMINIO_ID_Condominio`) REFERENCES `condominio` (`ID_Condominio`);

--
-- Filtros para la tabla `recarga`
--
ALTER TABLE `recarga`
  ADD CONSTRAINT `RECARGA_ESTANQUE_FK` FOREIGN KEY (`ESTANQUE_ID_Estanque`) REFERENCES `estanque` (`ID_Estanque`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
