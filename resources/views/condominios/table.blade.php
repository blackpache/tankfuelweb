<table class="table table-responsive" id="condominios-table">
    <thead>
        <tr>
        <th>Nombre de condominio</th>
        <th>Comuna responsable</th>
        <th>Dirección</th>
        <th>Contacto</th>
        <th>Telefono</th>
        <th>Hora de atención</th>
        <th colspan="3">Opciones</th>
        </tr>
    </thead>
    <tbody>
    @foreach($condominios as $condominio)
        <tr>
            <td>{!! $condominio->NombreCondominio !!}</td>
            <td>{!! $condominio->ComunaResponsable !!}</td>
            <td>{!! $condominio->Direccion !!}</td>
            <td>{!! $condominio->Contacto !!}</td>
            <td>{!! $condominio->Telefono !!}</td>
            <td>{!! $condominio->HoraAtencion !!}</td>
            <td>
                {!! Form::open(['route' => ['condominios.destroy', $condominio->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('condominios.show', [$condominio->id]) !!}" class='btn btn-default btn-xs'><i class="fa fa-info-circle" aria-hidden="true"></i></a>
                    <a href="{!! route('condominios.edit', [$condominio->id]) !!}" class='btn btn-default btn-xs'><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>