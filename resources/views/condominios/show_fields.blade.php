<!-- Nombrecondominio Field -->
<div class="form-group">
    {!! Form::label('NombreCondominio', 'Nombre de condominio:') !!}
    <p>{!! $condominio->NombreCondominio !!}</p>
</div>

<!-- Comunaresponsable Field -->
<div class="form-group">
    {!! Form::label('ComunaResponsable', 'Comuna responsable:') !!}
    <p>{!! $condominio->ComunaResponsable !!}</p>
</div>

<!-- Direccion Field -->
<div class="form-group">
    {!! Form::label('Direccion', 'Dirección:') !!}
    <p>{!! $condominio->Direccion !!}</p>
</div>

<!-- Contacto Field -->
<div class="form-group">
    {!! Form::label('Contacto', 'Contacto:') !!}
    <p>{!! $condominio->Contacto !!}</p>
</div>

<!-- Telefono Field -->
<div class="form-group">
    {!! Form::label('Telefono', 'Teléfono:') !!}
    <p>{!! $condominio->Telefono !!}</p>
</div>

<!-- Horaatencion Field -->
<div class="form-group">
    {!! Form::label('HoraAtencion', 'Hora de atención:') !!}
    <p>{!! $condominio->HoraAtencion !!}</p>
</div>


<div class="panel panel-default">
  <div class="panel-heading">Estanques</div>
  <div class="panel-body">Capacidad


<ul class="list-group">
  


@php
 foreach($estanques as $estanque)
            {
    echo " <li class='list-group-item'><a href='";
        echo route('estanques.show', [$estanque->id]);
        echo "'>$estanque->Nombre : $estanque->Capacidad</a></li>";
                
                
//echo route('estanques.edit', [$estanque->id]);

            }
@endphp
</ul>
</div>
    </div>
