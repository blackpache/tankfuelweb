
{!! $map['js'] !!}

<!-- Nombrecondominio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('NombreCondominio', 'Nombre de condominio:') !!}
    {!! Form::text('NombreCondominio', null, ['class' => 'form-control']) !!}
</div>

<!-- Comunaresponsable Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ComunaResponsable', 'Comuna responsable:') !!}
    {!! Form::text('ComunaResponsable', null, ['class' => 'form-control']) !!}
</div>

<!-- Direccion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Direccion', 'Direccion:') !!}
    {!! Form::text('Direccion', null, ['class' => 'form-control']) !!}
</div>

<!-- Contacto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Contacto', 'Contacto:') !!}
    {!! Form::text('Contacto', null, ['class' => 'form-control']) !!}
</div>

<!-- Telefono Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Telefono', 'Telefono:') !!}
    {!! Form::text('Telefono', null, ['class' => 'form-control']) !!}
</div>

<!-- Horaatencion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('HoraAtencion', 'Hora de atención:') !!}
    {!! Form::text('HoraAtencion', null, ['class' => 'form-control']) !!}
</div>

{{ Form::hidden('latitude', 'latitude', array('id' => 'latitude')) }}
{{ Form::hidden('longitude', 'longitude', array('id' => 'longitude')) }}


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('condominios.index') !!}" class="btn btn-default">Cancel</a>
</div>

