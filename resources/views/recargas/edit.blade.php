@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Recarga
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($recarga, ['route' => ['recargas.update', $recarga->id], 'method' => 'patch']) !!}

                        @include('recargas.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection