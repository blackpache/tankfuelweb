<!-- Id Recarga Field -->
<div class="form-group">
    {!! Form::label('ID_Recarga', 'Id Recarga:') !!}
    <p>{!! $recarga->ID_Recarga !!}</p>
</div>

<!-- Fecha Field -->
<div class="form-group">
    {!! Form::label('fecha', 'Fecha:') !!}
    <p>{!! $recarga->fecha !!}</p>
</div>

<!-- Porcentajeinicial Field -->
<div class="form-group">
    {!! Form::label('porcentajeInicial', 'Porcentajeinicial:') !!}
    <p>{!! $recarga->porcentajeInicial !!}</p>
</div>

<!-- Porcentajefinal Field -->
<div class="form-group">
    {!! Form::label('porcentajeFinal', 'Porcentajefinal:') !!}
    <p>{!! $recarga->porcentajeFinal !!}</p>
</div>

<!-- Litroscargados Field -->
<div class="form-group">
    {!! Form::label('litrosCargados', 'Litroscargados:') !!}
    <p>{!! $recarga->litrosCargados !!}</p>
</div>

<!-- Totallitros Field -->
<div class="form-group">
    {!! Form::label('totalLitros', 'Totallitros:') !!}
    <p>{!! $recarga->totalLitros !!}</p>
</div>

<!-- Estanque Id Estanque Field -->
<div class="form-group">
    {!! Form::label('ESTANQUE_ID_Estanque', 'Estanque Id Estanque:') !!}
    <p>{!! $recarga->ESTANQUE_ID_Estanque !!}</p>
</div>

