<!-- Estanque Id Estanque Field -->
<div class="form-group col-sm-6">
    
    {!! Form::label('ESTANQUE_ID_Estanque', 'Estanque Id Estanque:') !!}
   <select class="form-control" name="ESTANQUE_ID_Estanque">
    
    @php
       
        foreach($condominios as $condominio)
        {
            foreach($estanques as $estanque)
            {
                if ($estanque->CONDOMINIO_ID_Condominio == $condominio->id)
                {
                    $option =  $condominio->NombreCondominio ." : " . $estanque->Nombre;
                    echo("<option value='{$estanque->id}'>$option</option>");
                } 
            }
        }
    @endphp
  
    </select>
</div>

<!-- Fecha Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha', 'Fecha:') !!}
    {!! Form::date('fecha', null, ['class' => 'form-control']) !!}
</div>

<!-- Porcentajeinicial Field -->
<div class="form-group col-sm-6">
    {!! Form::label('porcentajeInicial', 'Porcentaje inicial:') !!}
    {!! Form::number('porcentajeInicial', null, ['class' => 'form-control']) !!}
</div>

<!-- Porcentajefinal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('porcentajeFinal', 'Porcentaje final:') !!}
    {!! Form::number('porcentajeFinal', null, ['class' => 'form-control']) !!}
</div>

<!-- Litroscargados Field -->
<div class="form-group col-sm-6">
    {!! Form::label('litrosCargados', 'Litros cargados:') !!}
    {!! Form::number('litrosCargados', null, ['class' => 'form-control']) !!}
</div>

<!-- Totallitros Field -->
<div class="form-group col-sm-6">
    {!! Form::label('totalLitros', 'Total de litros:') !!}
    {!! Form::number('totalLitros', null, ['class' => 'form-control']) !!}
</div>
<!-- Es_Urgente Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Es_Urgente', 'Tipo de recarga:') !!}

 <select class="form-control" name="Es_Urgente">
    <option value='0'>Normal</option>
    <option value='1'>Urgente</option>
 </select>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('recargas.index') !!}" class="btn btn-default">Cancel</a>
</div>
