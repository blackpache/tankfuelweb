<table class="table table-responsive" id="recargas-table">
    <thead>
        <tr>
        <th>Estanque</th>
        <th>Fecha</th>
        <th>Porcentaje inicial</th>
        <th>Porcentaje final</th>
        <th>Litros cargados</th>
        <th>Total de litros</th>
        <th>Tipo de Recarga</th>

        <!--
            <th colspan="3">Action</th>
        -->
        </tr>
    </thead>
    <tbody>
    @foreach($recargas as $recarga)

        <tr>
            @php
            
            foreach($condominios as $condominio)
            {
                foreach($estanques as $estanque)
                {
                    if ($estanque->CONDOMINIO_ID_Condominio==$condominio->id &&
                        $recarga->ESTANQUE_ID_Estanque == $estanque->id)
                    {
                        $option =  $condominio->NombreCondominio ." : " . $estanque->Nombre;
                        echo("<td>$option</td>");
                    }   
                }
            }
            @endphp
           
            <!--<td>{!! $recarga->ESTANQUE_ID_Estanque !!}</td>-->
            <td>{!! $recarga->fecha !!}</td>
            <td>{!! $recarga->porcentajeInicial !!}</td>
            <td>{!! $recarga->porcentajeFinal !!}</td>
            <td>{!! $recarga->litrosCargados !!}</td>
            <td>{!! $recarga->totalLitros !!}</td>
            @php
                if ($recarga->Es_Urgente)
                    echo("<td><i class='fa fa-exclamation-triangle' aria-hidden='true'></i></td>");
                else
                    echo("<td> </td>");
            @endphp
  
            <!--
            <td>
                {!! Form::open(['route' => ['recargas.destroy', $recarga->id], 'method' => 'delete']) !!}
              
                <div class='btn-group'>
                    <a href="{!! route('recargas.show', [$recarga->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('recargas.edit', [$recarga->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                
                {!! Form::close() !!}
            </td>
            -->
        </tr>
    @endforeach
    
    </tbody>
    
</table>