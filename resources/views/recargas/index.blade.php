@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Recargas</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('recargas.create') !!}">Add New</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <h4>Mostrando {{$recargas->count()}} recargas de un total de {{$recargas->total()}}</h4>
        <div class="box box-primary">
            <div class="box-body">
                    @include('recargas.table')
                    {{$recargas->links()}}
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

