@extends('layouts.app')

@section('content')

{!! $map['html'] !!}

<table class="table table-striped">
<thead>
    <tr>
        <th>Condominio</th>
        <th>Estanque</th>
        <th>Capacidad (L)</th>
        <th>Ultima recarga</th>
        <th>Remanente (%)</th>
        <th>Est. Proxima Recarga</th>
        <th>Alerta</th>
    </tr>
</thead>
<tbody>
    @php
    $rowCount=0;
    foreach ($condominios as $condominio) 
    {           
        echo ("<tr>");

        echo("<th scope='row'>{$condominio->NombreCondominio2}</th>");
        echo("<td>{$condominio->id_estanque}</td>");

        echo ("<td>{$condominio->capacidad}</td>");
        echo ("<td>{$condominio->fechaultima}</td>");
        echo ("<td>{$condominio->remanente}</td>");
        echo ("<td>{$condominio->fechaproxima}</td>");
        echo ("<td><i class='fa fa-exclamation-triangle' aria-hidden='true'></i></td>");
    
        echo ("</tr>");
    }

    @endphp
    
  
  </tbody>
</table>

{!! $map['js'] !!}


@endsection

