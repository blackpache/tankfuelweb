@extends('layouts.app')

@section('content')
<div class="jumbotron">
    <h1>Empresa X</h1> 
    <hr>
    <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec feugiat sem leo, ut bibendum eros varius ac. Praesent nec diam vel eros pulvinar suscipit. Ut non euismod nulla. Nulla facilisi. Sed dignissim ullamcorper efficitur. Quisque condimentum elit et risus mattis, at maximus nulla egestas. Proin efficitur bibendum dignissim. Cras efficitur sapien eu quam eleifend sodales. Integer convallis libero ipsum, sit amet finibus nulla mattis sed. Praesent bibendum porttitor malesuada. Fusce quis felis id est egestas sodales. Maecenas dictum, massa ac tempor imperdiet, elit libero semper diam, at pellentesque massa metus ut nisi. In dapibus varius laoreet. Vestibulum eu libero malesuada, egestas ex eu, gravida metus.
    </p>
    <p>
        Morbi eu congue risus. Proin vitae molestie eros. Sed ac finibus turpis. Sed semper sodales purus, vel dapibus odio suscipit et. Aenean tempus hendrerit augue, et pulvinar metus vehicula non. Vestibulum eu auctor eros. Integer egestas erat elit, non elementum lectus suscipit sed. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur mattis tristique egestas. Sed ut augue in nisl porta semper. Mauris mauris nibh, tincidunt vitae urna id, feugiat feugiat enim. In feugiat turpis quis finibus efficitur. Aliquam nec volutpat nisi. Morbi fringilla eros sed aliquam commodo. Vivamus eget nibh felis.
    </p> 
</div>
@endsection
