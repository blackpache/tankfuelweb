@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Temperaturas</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('temperaturas.create') !!}">Add New</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <h4>Mostrando {{$temperaturas->count()}} temperaturas de un total de {{$temperaturas->total()}}</h4>
        <div class="box box-primary">
            <div class="box-body">
                    @include('temperaturas.table')
                    {{$temperaturas->links()}}
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

