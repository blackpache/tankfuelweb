<!-- Fecha Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha', 'Fecha:') !!}
    {!! Form::date('fecha', null, ['class' => 'form-control']) !!}
</div>

<!-- Tempminima Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tempMinima', 'Temp. minima:') !!}
    {!! Form::number('tempMinima', null, ['class' => 'form-control']) !!}
</div>

<!-- Tempmax Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tempMax', 'Temp. maxima:') !!}
    {!! Form::number('tempMax', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('temperaturas.index') !!}" class="btn btn-default">Cancel</a>
</div>
