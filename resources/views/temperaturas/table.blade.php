<table class="table table-responsive" id="temperaturas-table">
    <thead>
        <tr>
        <th>Fecha</th>
        <th>Temp. minima</th>
        <th>Temp. maxima</th>
        <!--
            <th colspan="3">Action</th>
        -->
        </tr>
    </thead>
    <tbody>
    @foreach($temperaturas as $temperatura)
        <tr>
            <td>{!! $temperatura->fecha !!}</td>
            <td>{!! $temperatura->tempMinima !!}</td>
            <td>{!! $temperatura->tempMax !!}</td>
            <td>
                {!! Form::open(['route' => ['temperaturas.destroy', $temperatura->id], 'method' => 'delete']) !!}
                <!--
                <div class='btn-group'>
                    <a href="{!! route('temperaturas.show', [$temperatura->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('temperaturas.edit', [$temperatura->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
					
                </div>
                -->
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>