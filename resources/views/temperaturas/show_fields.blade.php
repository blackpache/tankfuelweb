<!-- Id Temperatura Field -->
<div class="form-group">
    {!! Form::label('ID_Temperatura', 'Id Temperatura:') !!}
    <p>{!! $temperatura->ID_Temperatura !!}</p>
</div>

<!-- Fecha Field -->
<div class="form-group">
    {!! Form::label('fecha', 'Fecha:') !!}
    <p>{!! $temperatura->fecha !!}</p>
</div>

<!-- Tempminima Field -->
<div class="form-group">
    {!! Form::label('tempMinima', 'Temp. minima:') !!}
    <p>{!! $temperatura->tempMinima !!}</p>
</div>

<!-- Tempmax Field -->
<div class="form-group">
    {!! Form::label('tempMax', 'Temp. maxima:') !!}
    <p>{!! $temperatura->tempMax !!}</p>
</div>

