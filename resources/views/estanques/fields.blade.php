<!-- Capacidad Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Capacidad', 'Capacidad:') !!}
    {!! Form::number('Capacidad', null, ['class' => 'form-control']) !!}
</div>

<!-- Condominio Id Condominio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('CONDOMINIO_ID_Condominio', 'Condominio:') !!}

    

      <select class="form-control" name="CONDOMINIO_ID_Condominio">
    
    @php
        foreach ($condominios as $condominio) 
          {
            echo("<option value='{$condominio->id}'>$condominio->NombreCondominio</option>");
          }

    @endphp
  
    </select>
    
    
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('estanques.index') !!}" class="btn btn-default">Cancel</a>
</div>
