<table class="table table-responsive" id="estanques-table">
    <thead>
        <tr>
            <th>Condominio</th>
            <th>Capacidad</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($estanques as $estanque)
        
        <tr>
            <td>
            @php
            foreach($condominios as $condominio)
            {
                if ($estanque->CONDOMINIO_ID_Condominio == $condominio->id)
                    {
                        echo $condominio->NombreCondominio;
                    }
            }
            @endphp
            </td>
            <td>{!! $estanque->Capacidad !!}</td>
            <td>
                {!! Form::open(['route' => ['estanques.destroy', $estanque->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('estanques.show', [$estanque->id]) !!}" class='btn btn-default btn-xs'><i class="fa fa-info-circle" aria-hidden="true"></i></a>
                    <a href="{!! route('estanques.edit', [$estanque->id]) !!}" class='btn btn-default btn-xs'><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>