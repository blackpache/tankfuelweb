<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('Nombre', 'Nombre:') !!}
    <p>{!! $estanque->Nombre !!}</p>
</div>
<!-- Capacidad Field -->
<div class="form-group">
    {!! Form::label('Capacidad', 'Capacidad:') !!}
    <p>{!! $estanque->Capacidad !!}</p>
</div>

<!-- Condominio Id Condominio Field -->
<div class="form-group">
    {!! Form::label('CONDOMINIO_ID_Condominio', 'Condominio:') !!}
    <p>{!! $condominio->NombreCondominio !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Creado en:') !!}
    <p>{!! $estanque->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Ultima actialización:') !!}
    <p>{!! $estanque->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Eliminado en:') !!}
    <p>{!! $estanque->deleted_at !!}</p>
</div>

