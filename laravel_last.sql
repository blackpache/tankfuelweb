-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-01-2018 a las 03:54:20
-- Versión del servidor: 10.1.28-MariaDB
-- Versión de PHP: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `laravel`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `condominio`
--

CREATE TABLE `condominio` (
  `id` int(11) NOT NULL,
  `NombreCondominio` char(100) NOT NULL,
  `ComunaResponsable` char(50) NOT NULL,
  `Direccion` char(100) NOT NULL,
  `Contacto` char(50) NOT NULL,
  `Telefono` char(15) NOT NULL,
  `HoraAtencion` char(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `condominio`
--

INSERT INTO `condominio` (`id`, `NombreCondominio`, `ComunaResponsable`, `Direccion`, `Contacto`, `Telefono`, `HoraAtencion`, `created_at`, `updated_at`, `deleted_at`, `latitude`, `longitude`) VALUES
(1, 'ProtoCondo', 'Viña', 'Viña', 'Viña', '00', '06', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '-33.253', '-71.465'),
(2, 'test', 'viña', 'asasdasd', 'asd', 'adasdasd', 'sadasd', '2017-12-16 23:55:26', '2017-12-16 23:55:26', NULL, '-33.223', '-71.265'),
(3, 'asd', 'asdasda', 'asdadsa', 'asdasd', 'asdasd', 'adasdasd', '2017-12-17 00:34:37', '2017-12-17 00:34:37', NULL, '-33.023', '-71.465'),
(4, 'aaa', 'aaa', 'aa', 'aaa', 'aa', 'aaa', '2017-12-17 00:36:02', '2017-12-17 23:54:58', '2017-12-17 23:54:58', '-33.123', '-71.565'),
(5, 'arlegui', 'z', 'ARLEGUI', '777', '777', '0', '2018-01-04 05:52:04', '2018-01-04 05:52:04', NULL, '-33.0239449', '-71.55182980000001'),
(6, '4 Orte', '4', '4 ORIENTE', '111', '11', '11', '2018-01-04 05:52:42', '2018-01-04 05:52:42', NULL, '-33.016191', '-71.54450329999997');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos`
--

CREATE TABLE `datos` (
  `ID_Dato` int(11) NOT NULL,
  `ID_Estanque` int(11) NOT NULL,
  `ID_Recarga` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `porcentajeInicial` decimal(9,4) NOT NULL,
  `porcentajeFinal` decimal(9,4) NOT NULL,
  `totalLitros` decimal(9,4) NOT NULL,
  `consPromDiario` decimal(9,4) NOT NULL,
  `precio` decimal(8,4) NOT NULL,
  `difDias` int(11) NOT NULL,
  `tempMin` decimal(6,3) NOT NULL,
  `PromTempMin` decimal(8,3) NOT NULL,
  `tempMax` decimal(6,3) NOT NULL,
  `PromTempMax` decimal(8,3) NOT NULL,
  `PromPrecio` decimal(8,4) NOT NULL,
  `contDias` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estanque`
--

CREATE TABLE `estanque` (
  `id` int(11) NOT NULL,
  `Capacidad` int(11) NOT NULL,
  `CONDOMINIO_ID_Condominio` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estanque`
--

INSERT INTO `estanque` (`id`, `Capacidad`, `CONDOMINIO_ID_Condominio`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 100, 3, '2017-12-18 00:51:29', '2017-12-18 05:17:19', NULL),
(2, 2, 3, '2017-12-18 00:55:35', '2017-12-18 05:23:41', NULL),
(3, 555, 3, '2017-12-18 04:56:40', '2017-12-18 04:56:40', NULL),
(4, 1000, 3, '2017-12-18 05:17:36', '2017-12-18 05:17:36', NULL),
(5, 1222, 2, '2017-12-18 05:21:11', '2017-12-18 05:21:11', NULL),
(6, 22, 2, '2017-12-18 05:22:16', '2017-12-18 05:22:16', NULL),
(7, 2, 2, '2017-12-18 05:22:37', '2017-12-18 05:22:37', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2017_12_16_213038_create_condominio_tests_table', 2),
('2017_12_16_221152_create_test_scaffolds_table', 3),
('2018_01_03_232355_add_latlong_info_to_condominio', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('altargot@gmail.com', '$2y$10$jtbWDnoSo7Gw4rHHZAU2Hu7r9iDAi3pTj8cFjOO6MRN1n.v2StAZS', '2017-12-17 03:55:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `precio`
--

CREATE TABLE `precio` (
  `ID_Precio` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `precio` decimal(8,4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recarga`
--

CREATE TABLE `recarga` (
  `id` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `porcentajeInicial` decimal(4,2) NOT NULL,
  `porcentajeFinal` decimal(4,2) NOT NULL,
  `litrosCargados` decimal(9,3) NOT NULL,
  `totalLitros` decimal(9,3) NOT NULL,
  `ESTANQUE_ID_Estanque` int(11) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `recarga`
--

INSERT INTO `recarga` (`id`, `fecha`, `porcentajeInicial`, `porcentajeFinal`, `litrosCargados`, `totalLitros`, `ESTANQUE_ID_Estanque`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '2017-12-06 00:00:00', '10.00', '25.00', '100.000', '200.000', 1, '2017-12-16', '2017-12-16', NULL),
(2, '2017-12-08 00:00:00', '2.00', '12.00', '2.000', '12.000', 1, '2017-12-16', '2017-12-16', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temperatura`
--

CREATE TABLE `temperatura` (
  `id` int(11) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL,
  `fecha` date NOT NULL,
  `tempMinima` decimal(6,3) NOT NULL,
  `tempMax` decimal(6,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `temperatura`
--

INSERT INTO `temperatura` (`id`, `created_at`, `updated_at`, `deleted_at`, `fecha`, `tempMinima`, `tempMax`) VALUES
(1, '2017-12-07', '2017-12-07', NULL, '2017-12-07', '6.000', '10.000'),
(2, '2017-12-07', '2017-12-07', NULL, '2017-12-13', '0.000', '10.000'),
(3, '2017-12-16', '2017-12-16', NULL, '2017-12-09', '12.000', '22.000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Chris Sevilleja', 'chris@scotch.io', '$2y$10$bc20RxXMOxbakC3lKEmKQOAiCd6j2CycavW.6592K6VOvzbUs/R0W', 'bFiLQoy2TBRL4R5MzeBqAA6OVnsebTjh89h8PWQBd4j88A03767jScunDH33', '2017-11-29 04:30:33', '2017-11-29 04:30:33'),
(2, 'Usuario1', 'usuario1@gmail.com', '$2y$10$bg75uEo8GfQrvVaKJhwy7OMnNYK.u8Z/NygbteC4JlW0L9Ao6CBk.', 'Drn7ikiYax6uwdoFBiszemabloOjQ02FFYzOYlghcca97zRfEr19bpoIjOAW', '2017-11-30 07:00:00', '2017-11-30 07:00:00'),
(3, 'usuario2', 'usuario2@gmail.com', '$2y$10$/jKV4CmLokBCRRJk2wSBuuN8UmgHEd9GXwdiHqS/p9H.pPTSTnO.G', 'syHjXCoLJ6AloV3aHSl1R8nBTkaijiVV9JYNrELeyxqEZIW7hWyrppz8vWcb', '2017-11-30 07:00:38', '2017-11-30 07:00:38'),
(4, 'jt', 'altargot@gmail.com', '$2y$10$VKn78yAV5rpic8XlJ4NbKuUtXh8UGNhFtxXWYRDumkewXvyarD7U.', '5mNn773NWLLNgQ8MnN1Ol8dU7OKM3g1niG6MUpvmsQUn35yP3nTVM214MfgY', '2017-12-17 03:55:27', '2017-12-17 03:55:27');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `condominio`
--
ALTER TABLE `condominio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `datos`
--
ALTER TABLE `datos`
  ADD PRIMARY KEY (`ID_Dato`);

--
-- Indices de la tabla `estanque`
--
ALTER TABLE `estanque`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `precio`
--
ALTER TABLE `precio`
  ADD PRIMARY KEY (`ID_Precio`);

--
-- Indices de la tabla `recarga`
--
ALTER TABLE `recarga`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `temperatura`
--
ALTER TABLE `temperatura`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `condominio`
--
ALTER TABLE `condominio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `estanque`
--
ALTER TABLE `estanque`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `recarga`
--
ALTER TABLE `recarga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `temperatura`
--
ALTER TABLE `temperatura`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
