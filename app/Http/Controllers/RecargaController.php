<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRecargaRequest;
use App\Http\Requests\UpdateRecargaRequest;
use App\Repositories\RecargaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\DB;
class RecargaController extends AppBaseController
{
    /** @var  RecargaRepository */
    private $recargaRepository;

    public function __construct(RecargaRepository $recargaRepo)
    {
        $this->middleware('auth');
        $this->recargaRepository = $recargaRepo;
    }

    /**
     * Display a listing of the Recarga.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->recargaRepository->pushCriteria(new RequestCriteria($request));
        $condominios = DB::table('condominio')->select('NombreCondominio', 'id')->whereNull('deleted_at')->get();
        $estanques = DB::table('estanque')->select('Nombre', 'id','CONDOMINIO_ID_Condominio')->whereNull('deleted_at')->get();
        $recargas = $this->recargaRepository->paginate(5);

        return view('recargas.index')
            ->with('recargas', $recargas)
            ->with('condominios', $condominios)
            ->with('estanques', $estanques);
    }

    /**
     * Show the form for creating a new Recarga.
     *
     * @return Response
     */
    public function create()
    {
        $condominios = DB::table('condominio')->select('NombreCondominio', 'id')->whereNull('deleted_at')->get();
        $estanques = DB::table('estanque')->select('Nombre', 'id','CONDOMINIO_ID_Condominio')->whereNull('deleted_at')->get();
        return view('recargas.create')
        ->with('condominios', $condominios)
        ->with('estanques', $estanques);;
    }

    /**
     * Store a newly created Recarga in storage.
     *
     * @param CreateRecargaRequest $request
     *
     * @return Response
     */
    public function store(CreateRecargaRequest $request)
    {
        $input = $request->all();

        $recarga = $this->recargaRepository->create($input);

        Flash::success('Recarga saved successfully.');

        return redirect(route('recargas.index'));
    }

    /**
     * Display the specified Recarga.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $recarga = $this->recargaRepository->findWithoutFail($id);

        if (empty($recarga)) {
            Flash::error('Recarga not found');

            return redirect(route('recargas.index'));
        }

        return view('recargas.show')->with('recarga', $recarga);
    }

    /**
     * Show the form for editing the specified Recarga.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $recarga = $this->recargaRepository->findWithoutFail($id);

        if (empty($recarga)) {
            Flash::error('Recarga not found');

            return redirect(route('recargas.index'));
        }

        return view('recargas.edit')->with('recarga', $recarga);
    }

    /**
     * Update the specified Recarga in storage.
     *
     * @param  int              $id
     * @param UpdateRecargaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRecargaRequest $request)
    {
        $recarga = $this->recargaRepository->findWithoutFail($id);

        if (empty($recarga)) {
            Flash::error('Recarga not found');

            return redirect(route('recargas.index'));
        }

        $recarga = $this->recargaRepository->update($request->all(), $id);

        Flash::success('Recarga updated successfully.');

        return redirect(route('recargas.index'));
    }

    /**
     * Remove the specified Recarga from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $recarga = $this->recargaRepository->findWithoutFail($id);

        if (empty($recarga)) {
            Flash::error('Recarga not found');

            return redirect(route('recargas.index'));
        }

        $this->recargaRepository->delete($id);

        Flash::success('Recarga deleted successfully.');

        return redirect(route('recargas.index'));
    }
}
