<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTemperaturaRequest;
use App\Http\Requests\UpdateTemperaturaRequest;
use App\Repositories\TemperaturaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class TemperaturaController extends AppBaseController
{
    /** @var  TemperaturaRepository */
    private $temperaturaRepository;

    public function __construct(TemperaturaRepository $temperaturaRepo)
    {
        $this->middleware('auth');
        $this->temperaturaRepository = $temperaturaRepo;
    }

    /**
     * Display a listing of the Temperatura.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->temperaturaRepository->pushCriteria(new RequestCriteria($request));
        $temperaturas = $this->temperaturaRepository->paginate(5);

        return view('temperaturas.index')
            ->with('temperaturas', $temperaturas);
    }

    /**
     * Show the form for creating a new Temperatura.
     *
     * @return Response
     */
    public function create()
    {
        return view('temperaturas.create');
    }

    /**
     * Store a newly created Temperatura in storage.
     *
     * @param CreateTemperaturaRequest $request
     *
     * @return Response
     */
    public function store(CreateTemperaturaRequest $request)
    {
        $input = $request->all();

        $temperatura = $this->temperaturaRepository->create($input);

        Flash::success('Temperatura saved successfully.');

        return redirect(route('temperaturas.index'));
    }

    /**
     * Display the specified Temperatura.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $temperatura = $this->temperaturaRepository->findWithoutFail($id);

        if (empty($temperatura)) {
            Flash::error('Temperatura not found');

            return redirect(route('temperaturas.index'));
        }

        return view('temperaturas.show')->with('temperatura', $temperatura);
    }

    /**
     * Show the form for editing the specified Temperatura.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $temperatura = $this->temperaturaRepository->findWithoutFail($id);

        if (empty($temperatura)) {
            Flash::error('Temperatura not found');

            return redirect(route('temperaturas.index'));
        }

        return view('temperaturas.edit')->with('temperatura', $temperatura);
    }

    /**
     * Update the specified Temperatura in storage.
     *
     * @param  int              $id
     * @param UpdateTemperaturaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTemperaturaRequest $request)
    {
        $temperatura = $this->temperaturaRepository->findWithoutFail($id);

        if (empty($temperatura)) {
            Flash::error('Temperatura not found');

            return redirect(route('temperaturas.index'));
        }

        $temperatura = $this->temperaturaRepository->update($request->all(), $id);

        Flash::success('Temperatura updated successfully.');

        return redirect(route('temperaturas.index'));
    }

    /**
     * Remove the specified Temperatura from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $temperatura = $this->temperaturaRepository->findWithoutFail($id);

        if (empty($temperatura)) {
            Flash::error('Temperatura not found');

            return redirect(route('temperaturas.index'));
        }

        $this->temperaturaRepository->delete($id);

        Flash::success('Temperatura deleted successfully.');

        return redirect(route('temperaturas.index'));
    }
}
