<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCondominioRequest;
use App\Http\Requests\UpdateCondominioRequest;
use App\Repositories\CondominioRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

use Illuminate\Support\Facades\DB;

class CondominioController extends AppBaseController
{
    /** @var  CondominioRepository */
    private $condominioRepository;

    public function __construct(CondominioRepository $condominioRepo)
    {
        $this->middleware('auth');
        $this->condominioRepository = $condominioRepo;
    }

    /**
     * Display a listing of the Condominio.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->condominioRepository->pushCriteria(new RequestCriteria($request));
        $condominios = $this->condominioRepository->paginate(5);

        return view('condominios.index')
            ->with('condominios', $condominios);
    }

    /**
     * Show the form for creating a new Condominio.
     *
     * @return Response
     */
    public function create()
    {
         //Googlemap
        $config = array();
        $config['center'] = 'auto';

        app('map')->initialize($config);
        $map = app('map')->create_map();
        $geocodeScript = "
        <script>
        window.addEventListener('load', function() {
  console.log('All assets are loaded');
  document.getElementById('Direccion').onchange = function() {myFunction()};
})
        


function myFunction() {
    var x = document.getElementById('Direccion');
    x.value = x.value.toUpperCase();
    geocodeAddress(new google.maps.Geocoder());
}


function geocodeAddress(geocoder, resultsMap) {
        var address = document.getElementById('Direccion').value;
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            document.getElementById('latitude').value = results[0].geometry.location.lat();
            document.getElementById('longitude').value = results[0].geometry.location.lng();
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }


</script>
";
        return view('condominios.create')->with('geocodeScript', $geocodeScript)->with('map', $map);
    }

    /**
     * Store a newly created Condominio in storage.
     *
     * @param CreateCondominioRequest $request
     *
     * @return Response
     */
    public function store(CreateCondominioRequest $request)
    {
        $input = $request->all();

        $condominio = $this->condominioRepository->create($input);

        Flash::success('Condominio saved successfully.');

        return redirect(route('condominios.index'));
    }

    /**
     * Display the specified Condominio.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $condominio = $this->condominioRepository->findWithoutFail($id);

        $estanques = DB::table('estanque')->where('CONDOMINIO_ID_Condominio', '=', $id)->get();

        
        if (empty($condominio)) {
            Flash::error('Condominio not found');

            return redirect(route('condominios.index'));
        }

        return view('condominios.show')->with('condominio', $condominio)->with('estanques', $estanques);
    }

    /**
     * Show the form for editing the specified Condominio.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $condominio = $this->condominioRepository->findWithoutFail($id);

        if (empty($condominio)) {
            Flash::error('Condominio not found');

            return redirect(route('condominios.index'));
        }

        return view('condominios.edit')->with('condominio', $condominio);
    }

    /**
     * Update the specified Condominio in storage.
     *
     * @param  int              $id
     * @param UpdateCondominioRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCondominioRequest $request)
    {
        $condominio = $this->condominioRepository->findWithoutFail($id);

        if (empty($condominio)) {
            Flash::error('Condominio not found');

            return redirect(route('condominios.index'));
        }

        $condominio = $this->condominioRepository->update($request->all(), $id);

        Flash::success('Condominio updated successfully.');

        return redirect(route('condominios.index'));
    }

    /**
     * Remove the specified Condominio from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $condominio = $this->condominioRepository->findWithoutFail($id);

        if (empty($condominio)) {
            Flash::error('Condominio not found');

            return redirect(route('condominios.index'));
        }

        $this->condominioRepository->delete($id);

        Flash::success('Condominio deleted successfully.');

        return redirect(route('condominios.index'));
    }
}
