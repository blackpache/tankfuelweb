<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Googlemap
        $config = array();
        $config['center'] = 'auto';

        app('map')->initialize($config);

		$resultadosTable = DB::table('resultadosmil')->get();
		
		$idestanque=0;
		$capacidad=0;
		$fechaultima = "";
		$remanente=0;
		$fechaproxima = "";
		
        foreach ($resultadosTable as $resultados) 
        {   
			
			$condominiosTable = DB::table('condominio')->where('id', '=', $resultados->id_condominio)->get();
			foreach($condominiosTable as $condominio)
			{
				$marker = array();
				$nombrecondominio = $condominio->NombreCondominio;
				
				$marker['position'] = $condominio->latitude.', '.$condominio->longitude;
				$marker['title'] = $condominio->NombreCondominio;
				app('map')->add_marker($marker);
			}
			$resultados->NombreCondominio2 = $nombrecondominio;
			
			$resultados-> id_estanque = $resultados->id_estanque;
			$resultados-> capacidad = $resultados->capacidad_estanque;
			$resultados-> fechaultima = $resultados->fecha_recarga;
			$resultados-> remanente = $resultados->porcentajeActual;
			$resultados-> fechaproxima = $resultados->fechaProxRecarga;
			
        }
        

        $map = app('map')->create_map();
        
        //$estanquesTable = DB::table('condominio')->where('CONDOMINIO_ID_Condominio', '=', $id)->get();
        return view('home')->with('condominios', $resultadosTable)->with('map', $map);
    }
}