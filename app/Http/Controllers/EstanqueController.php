<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEstanqueRequest;
use App\Http\Requests\UpdateEstanqueRequest;
use App\Repositories\EstanqueRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

use Illuminate\Support\Facades\DB;

class EstanqueController extends AppBaseController
{
    /** @var  EstanqueRepository */
    private $estanqueRepository;

    public function __construct(EstanqueRepository $estanqueRepo)
    {
        $this->estanqueRepository = $estanqueRepo;
    }

    /**
     * Display a listing of the Estanque.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->estanqueRepository->pushCriteria(new RequestCriteria($request));
        $estanques = $this->estanqueRepository->paginate(5);
      
        $condominios = DB::table('condominio')->select('NombreCondominio', 'id')->whereNull('deleted_at')->get();
        
        return view('estanques.index')
            ->with('estanques', $estanques)->with('condominios', $condominios);
    }

    /**
     * Show the form for creating a new Estanque.
     *
     * @return Response
     */
    public function create()
    {
        $condominios = DB::table('condominio')->select('NombreCondominio', 'id')->whereNull('deleted_at')->get();
        
        return view('estanques.create')
            ->with('condominios', $condominios);
    }

    /**
     * Store a newly created Estanque in storage.
     *
     * @param CreateEstanqueRequest $request
     *
     * @return Response
     */
    public function store(CreateEstanqueRequest $request)
    {
        $input = $request->all();

        $estanque = $this->estanqueRepository->create($input);

        Flash::success('Estanque saved successfully.');

        return redirect(route('estanques.index'));
    }

    /**
     * Display the specified Estanque.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $estanque = $this->estanqueRepository->findWithoutFail($id);
        $condominio = DB::table('condominio')->where('id',$estanque->CONDOMINIO_ID_Condominio)->first();
        if (empty($estanque)) {
            Flash::error('Estanque not found');

            return redirect(route('estanques.index'));
        }

        return view('estanques.show')->with('estanque', $estanque)->with('condominio', $condominio);
    }

    /**
     * Show the form for editing the specified Estanque.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $estanque = $this->estanqueRepository->findWithoutFail($id);
         $condominios = DB::table('condominio')->select('NombreCondominio', 'id')->whereNull('deleted_at')->get();
        
        
        if (empty($estanque)) {
            Flash::error('Estanque not found');

            return redirect(route('estanques.index'));
        }

        return view('estanques.edit')->with('estanque', $estanque)->with('condominios', $condominios);
    }

    /**
     * Update the specified Estanque in storage.
     *
     * @param  int              $id
     * @param UpdateEstanqueRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEstanqueRequest $request)
    {
        $estanque = $this->estanqueRepository->findWithoutFail($id);

        if (empty($estanque)) {
            Flash::error('Estanque not found');

            return redirect(route('estanques.index'));
        }

        $estanque = $this->estanqueRepository->update($request->all(), $id);

        Flash::success('Estanque updated successfully.');

        return redirect(route('estanques.index'));
    }

    /**
     * Remove the specified Estanque from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $estanque = $this->estanqueRepository->findWithoutFail($id);

        if (empty($estanque)) {
            Flash::error('Estanque not found');

            return redirect(route('estanques.index'));
        }

        $this->estanqueRepository->delete($id);

        Flash::success('Estanque deleted successfully.');

        return redirect(route('estanques.index'));
    }
}
