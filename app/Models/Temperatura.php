<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Temperatura
 * @package App\Models
 * @version December 7, 2017, 2:48 am UTC
 *
 * @property date fecha
 * @property decimal tempMinima
 * @property decimal tempMax
 */
class Temperatura extends Model
{
    use SoftDeletes;

    public $table = 'temperatura';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'fecha',
        'tempMinima',
        'tempMax'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'ID_Temperatura' => 'integer',
        'fecha' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
