<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Recarga
 * @package App\Models
 * @version December 7, 2017, 3:15 am UTC
 *
 * @property \App\Models\Estanque estanque
 * @property string|\Carbon\Carbon fecha
 * @property decimal porcentajeInicial
 * @property decimal porcentajeFinal
 * @property decimal litrosCargados
 * @property decimal totalLitros
 * @property integer ESTANQUE_ID_Estanque
 */
class Recarga extends Model
{
    use SoftDeletes;

    public $table = 'recarga';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'fecha',
        'porcentajeInicial',
        'porcentajeFinal',
        'litrosCargados',
        'totalLitros',
        'Es_Urgente',
        'ESTANQUE_ID_Estanque'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'ID_Recarga' => 'integer',
        'ESTANQUE_ID_Estanque' => 'integer',
        'Es_Urgente' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function estanque()
    {
        return $this->belongsTo(\App\Models\Estanque::class);
    }
}
