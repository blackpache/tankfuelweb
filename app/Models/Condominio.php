<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Condominio
 * @package App\Models
 * @version December 16, 2017, 8:50 pm UTC
 *
 * @property string NombreCondominio
 * @property string ComunaResponsable
 * @property string Direccion
 * @property string Contacto
 * @property string Telefono
 * @property string HoraAtencion
 */
class Condominio extends Model
{
    use SoftDeletes;

    public $table = 'condominio';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'NombreCondominio',
        'ComunaResponsable',
        'Direccion',
        'Contacto',
        'Telefono',
        'HoraAtencion',
        'latitude',
        'longitude'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'ID_Condominio' => 'integer',
        'NombreCondominio' => 'string',
        'ComunaResponsable' => 'string',
        'Direccion' => 'string',
        'Contacto' => 'string',
        'Telefono' => 'string',
        'HoraAtencion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
