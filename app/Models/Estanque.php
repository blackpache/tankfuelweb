<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Estanque
 * @package App\Models
 * @version December 17, 2017, 9:41 pm UTC
 *
 * @property integer Capacidad
 * @property integer CONDOMINIO_ID_Condominio
 */
class Estanque extends Model
{
    use SoftDeletes;

    public $table = 'estanque';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'Capacidad',
        'CONDOMINIO_ID_Condominio',
        'Nombre'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'ID_Estanque' => 'integer',
        'Capacidad' => 'integer',
        'CONDOMINIO_ID_Condominio' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
