<?php

namespace App\Repositories;

use App\Models\Estanque;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class EstanqueRepository
 * @package App\Repositories
 * @version December 17, 2017, 9:41 pm UTC
 *
 * @method Estanque findWithoutFail($id, $columns = ['*'])
 * @method Estanque find($id, $columns = ['*'])
 * @method Estanque first($columns = ['*'])
*/
class EstanqueRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'Capacidad',
        'CONDOMINIO_ID_Condominio'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Estanque::class;
    }
}
