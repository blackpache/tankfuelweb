<?php

namespace App\Repositories;

use App\Models\Recarga;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RecargaRepository
 * @package App\Repositories
 * @version December 7, 2017, 3:15 am UTC
 *
 * @method Recarga findWithoutFail($id, $columns = ['*'])
 * @method Recarga find($id, $columns = ['*'])
 * @method Recarga first($columns = ['*'])
*/
class RecargaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'fecha',
        'porcentajeInicial',
        'porcentajeFinal',
        'litrosCargados',
        'totalLitros',
        'ESTANQUE_ID_Estanque'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Recarga::class;
    }
}
