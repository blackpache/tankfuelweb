<?php

namespace App\Repositories;

use App\Models\Condominio;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CondominioRepository
 * @package App\Repositories
 * @version December 16, 2017, 8:50 pm UTC
 *
 * @method Condominio findWithoutFail($id, $columns = ['*'])
 * @method Condominio find($id, $columns = ['*'])
 * @method Condominio first($columns = ['*'])
*/
class CondominioRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'NombreCondominio',
        'ComunaResponsable',
        'Direccion',
        'Contacto',
        'Telefono',
        'HoraAtencion'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Condominio::class;
    }
}
