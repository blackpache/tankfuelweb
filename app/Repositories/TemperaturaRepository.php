<?php

namespace App\Repositories;

use App\Models\Temperatura;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TemperaturaRepository
 * @package App\Repositories
 * @version December 7, 2017, 2:48 am UTC
 *
 * @method Temperatura findWithoutFail($id, $columns = ['*'])
 * @method Temperatura find($id, $columns = ['*'])
 * @method Temperatura first($columns = ['*'])
*/
class TemperaturaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'fecha',
        'tempMinima',
        'tempMax'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Temperatura::class;
    }
}
